
// sticky nav plugin activation


 $("#sticker").sticky({topSpacing:0});

/* mixitup plugin activation */
var mixer = mixitup('.gallery-items');


/* owl-carousel plugin activation */
  $(".owl-carousel").owlCarousel({

  	 items:5,
  	 margin:15,
  	 loop:true,
  	 autoplay:true,
     autoplayTimeout:1000,
     autoplayHoverPause:true,
     responsive : {
    // breakpoint from 0 up
		    0 : {
		        items:1
		       
		    },
		    // breakpoint from 480 up
		    480 : {
		         items:3
		    },
		    // breakpoint from 768 up
		    768 : {
		       items:5
		    }
		}
  });


  /* google map js activation */

   var map = new GMaps({
      el: '#map',
      lat: 23.7510069,
      lng:90.3874714
    });
  	map.addMarker({
        lat: 23.7510069,
        lng:90.3874714,
        title: 'Lima',
        details: {
          database_id: 42,
          author: 'HPNeo'
        },
        click: function(e){
          if(console.log)
            console.log(e);
          alert('You clicked in this marker');
        },
        mouseover: function(e){
          if(console.log)
            console.log(e);
        }
      });
